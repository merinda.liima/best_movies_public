module ApplicationHelper

    def genres
        url = "#{ENV['BASE_URL']}#{ENV['GENRES_URL']}#{ENV['API_KEY']}&language=pt-BR"
        @genres_hash = Movies.utils_service(url)
        @genres = @genres_hash.select{|m| m['genres']}['genres']
    end    
end
