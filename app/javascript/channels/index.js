// Load all the channels within this directory and all subdirectories.
// Channel files must be named *_channel.js.
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery3
//= require popper
//= require bootstrap
//= require_tree .

const channels = require.context('.', true, /_channel\.js$/)
channels.keys().forEach(channels)
