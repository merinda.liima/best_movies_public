module Movies
    extend ActiveSupport::Concern

    def self.utils_service(url)
        @service = HTTParty.get(url)
        @hash = JSON.parse(@service.body)
    end    
end  