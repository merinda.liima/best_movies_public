class MoviesController < ApplicationController

  def index
    url = "#{ENV['BASE_URL']}#{ENV['API_URL']}#{ENV['API_KEY']}"
    @movies_hash = Movies.utils_service(url)
    @movies = @movies_hash.select{|m| m['results']}['results']
  end

  def search
    url = "#{ENV['BASE_URL']}#{ENV['SEARCH_URL']}#{ENV['API_KEY']}&query=#{params[:movie]}"
    @movies_hash = Movies.utils_service(url)
    @movies = @movies_hash.select{|m| m['results']}['results']
    render "index"
  end

  def releases
    url = "#{ENV['BASE_URL']}#{ENV['RELEASES_URL']}#{ENV['API_KEY']}&language=en-US"
    @movies_hash = Movies.utils_service(url)
    @movies = @movies_hash.select{|m| m['results']}['results']
    render "index"
  end
  
  def top10
    url = "#{ENV['BASE_URL']}#{ENV['TOP10_URL']}#{ENV['API_KEY']}&language=en-US&page=1"
    @movies_hash = Movies.utils_service(url)
    @movies = @movies_hash.select{|m| m['results']}['results']
    render "index"
  end

  def genders
    url = "#{ENV['BASE_URL']}#{ENV['API_URL']}#{ENV['API_KEY']}&with_genres=#{params[:id]}"
    @movies_hash = Movies.utils_service(url)
    @movies = @movies_hash.select{|m| m['results']}['results']
    render "index"
  end
  
end
