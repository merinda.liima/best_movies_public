Rails.application.routes.draw do
  root "movies#index"

  get "/movies", to: "movies#index"
  get "/movies/search", to: "movies#search"
  get "/movies/releases", to: "movies#releases"
  get "/movies/top10", to: "movies#top10"
  get "/movies/genders/:id", to: "movies#genders"
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
